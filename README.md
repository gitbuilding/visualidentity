# GitBuilding Visual Identity

This project holds the logo for GitBuilding and the all the GitBuilding sub projects.

The Logo was designed by Charlotte Parry as a parody of the Git Logo. To be clear GitBuilding is not affiliated with the official Git project.

![](GitBuilding.svg)
